val mymain = Some("com.bitbucket.trylks.katas.mmxv.iii.PickGuess")

lazy val root = (project in file(".")).
  settings(
    name := "pickguess",
    version := "1.0",
    scalaVersion := "2.11.5",
    libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % Test,   
    mainClass in Compile := mymain ,
    mainClass in assembly := mymain ,
    mainClass in run := mymain
  )
  
  
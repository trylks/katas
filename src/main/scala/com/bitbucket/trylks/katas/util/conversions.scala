package com.bitbucket.trylks.katas.util

import scala.annotation.tailrec

object conversions {

  // make general base convert with decorator for tolower toupper methods.
  @tailrec
  def hex2dec(s: String, acc: Int = 0): Int = {
    if (s.isEmpty)
      acc
    else
      hex2dec(s.tail, acc * 16 + digitConvert(s.head))
  }

  private def digitConvert(c: Char): Int = c.toLower match {
    case 'a' => 10
    case 'b' => 11
    case 'c' => 12
    case 'd' => 13
    case 'e' => 14
    case 'f' => 15
    case a   => (a - '0').toInt
  }

}

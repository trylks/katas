package com.bitbucket.trylks.katas

// "convenience" is a way to say: I should not be doing this
object convenience {

  var nothing: Nothing = _

}

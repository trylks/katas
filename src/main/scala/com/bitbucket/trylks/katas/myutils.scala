package com.bitbucket.trylks.katas

import scala.compat.Platform
import scala.util.Random
import scala.collection.GenSeq
import scala.annotation.tailrec

object myutils {

  private val randomGen = new Random(Platform.currentTime)

  def random[T](options: GenSeq[T]): Option[T] =
    if (options.isEmpty)
      None
    else
      Some(options(randomGen.nextInt(options.size)))

  def randomBool(): Boolean = randomGen.nextBoolean()

  def randomInt(range: (Int, Int)): Int = randomGen.nextInt(range._2 - range._1 + 1) + range._1

  def randomWeight(weights: Seq[Int]): Int = chooseWeight(randomGen.nextInt(weights.sum), weights)

  @tailrec
  private def chooseWeight(choose: Int, weights: Seq[Int], result: Int = 0): Int =
    if (choose < weights.head)
      result
    else
      chooseWeight(choose - weights.head, weights.tail, result + 1)
}

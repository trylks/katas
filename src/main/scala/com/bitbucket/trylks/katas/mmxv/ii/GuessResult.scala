package com.bitbucket.trylks.katas.mmxv.ii

object GuessResult extends Enumeration {
  type GuessResult = Value
  val noGuess, guessGreater, guessLesser, guessEqual = Value
}

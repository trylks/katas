package com.bitbucket.trylks.katas.mmxv.iv

import scala.annotation.tailrec

object Main extends App {

  def isPrime(n: Long): Boolean = primeNumbers.takeWhile(_ <= Math.sqrt(n)).forall(n % _ != 0)
  val primeNumbers = 2 #:: (3.toLong to Int.MaxValue by 2).iterator.filter(isPrime).toStream

  @tailrec
  def decompose(n: Long, acc: Seq[Long] = Seq(), i: Int = 0): Seq[Long] =
    if (n == 1)
      acc
    else if (primeNumbers(i) > Math.sqrt(n))
      acc :+ n
    else if (n % primeNumbers(i) == 0)
      decompose(n / primeNumbers(i), acc :+ primeNumbers(i), i)
    else
      decompose(n, acc, i + 1)

  def lcm(ns: Seq[Long]): Long = ns.map(decompose(_)).reduce((x, y) => x ++ (y diff x)).reduceLeft(_ * _)
  def gcd(ns: Seq[Long]): Long = ns.map(decompose(_)).reduce((x, y) => x intersect y).reduceLeft(_ * _)

  val inn = args.map(_.toLong).toSeq
  println(s"The least common multiple is ${lcm(inn)}")
  println(s"The greatest common divisor is ${gcd(inn)}")

}

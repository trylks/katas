package com.bitbucket.trylks.katas.mmxv.iii

import scala.compat.Platform
import scala.util.Random
import scala.collection.mutable.ArrayOps
import scala.annotation.tailrec

class Selector[T](options: Seq[T]) {

  private val randomGen = new Random(Platform.currentTime)
  private val successes: ArrayOps[Int] = Iterator.continually(1).take(options.size).toArray
  private val failures: ArrayOps[Int] = Iterator.continually(1).take(options.size).toArray
  private val deadpool = 50 // healing through forgetting (as the state changes, the quality of the strategies may change)

  def update(option: Int, success: Int): Unit = {
    if (success > 0)
      successes(option) += success
    else
      failures(option) -= success
    if (successes(option) >= deadpool && failures(option) >= deadpool) {
      val d = mcd(successes(option), failures(option))
      successes(option) /= d
      failures(option) /= d
    }
  }

  def choose(): (T, Int) = {
    val weights = (0 until options.size).map(
      i => ((1000.0 * successes(i)) / (successes(i) + failures(i))).ceil.toInt
    )
    val cumulative = weights.tail.scanLeft(weights.head)(_ + _)
    val chosen = randomGen.nextInt(cumulative.last)
    val result = cumulative.takeWhile(_ <= chosen).size
    (options(result), result)
  }

  @tailrec
  private final def mcd(a: Int, b: Int): Int =
    if (b == 0)
      a
    else
      mcd(b, a % b)

}

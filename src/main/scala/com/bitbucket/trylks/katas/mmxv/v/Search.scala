package com.bitbucket.trylks.katas.mmxv.v

import scala.annotation.tailrec

object Search {

  def backtracking[A](children: A => Seq[A])(isFinal: A => Boolean)(start: A) = {
    @tailrec
    def recbt(current: Seq[A], acc: Seq[A]): Seq[A] =
      if (current.isEmpty)
        current
      else if (isFinal(current.head))
        recbt(current.tail, acc :+ current.head)
      else
        recbt(children(current.head) ++ current.tail, acc)
    recbt(Seq(start), Seq.empty[A])
  }

}

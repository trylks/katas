package com.bitbucket.trylks.katas.mmxv.ii

import scala.util.Random
import GuessResult._
import com.bitbucket.trylks.katas.myutils

// alternatively, this could be a set of classes implementing a single interface
object GuessStrategies {

  val totalAttempts = Configuration.getAttempts()
  def ideally(remaining: Int) = remaining * remaining - 1
  type guessStrategy = (Int, Int, Int, GuessResult) => Int // I'd like to put the parameter names here, for semantic purposes

  class NamedGuessStrategy(strategy: guessStrategy, val name: String) extends guessStrategy {
    def apply(start: Int, end: Int, attempt: Int, previous: GuessResult.GuessResult): Int = strategy(start, end, attempt, previous)
  }

  val basic: guessStrategy = (start, end, attempt, previous) => {
    val sign = previous match {
      case GuessResult.guessGreater => 1
      case GuessResult.guessLesser  => -1
      case _                        => 0
    }
    (start + end) / 2 + sign * 2 * (totalAttempts - attempt)
  }

  val binary: guessStrategy = (start, end, attempt, previous) => (start + end) / 2

  val feelingLucky: (Int, Int, Int, GuessResult) => Int = (start, end, attempt, previous) => previous match {
    case GuessResult.guessGreater => end - ideally(totalAttempts - attempt)
    case GuessResult.guessLesser  => start + ideally(totalAttempts - attempt)
    case _                        => 50
  }

  val odds: guessStrategy = (start, end, attempt, previous) =>
    if (attempt % 2 != 0 && attempt != totalAttempts)
      binary(start, end, attempt, previous)
    else previous match {
      case GuessResult.guessGreater => end
      case GuessResult.guessLesser  => start
    }

  val slipknot: guessStrategy = (start, end, attempt, previous) => {
    val remaining = totalAttempts - attempt
    val margin = ((attempt * ideally(remaining) + remaining * (end - start) / 2.0) / totalAttempts).round.toInt
    previous match {
      case GuessResult.guessGreater => end - margin
      case GuessResult.guessLesser  => start + margin
      case _                        => 50
    }
  }

  val randomSlipknot: guessStrategy = (start, end, attempt, previous) => {
    val remaining = totalAttempts - attempt
    val slipknot = (attempt * ideally(remaining) + remaining * (end - start) / 2.0) / totalAttempts
    val randomizedMargin =
      (if (myutils.randomBool())
        end - start - slipknot
      else
        slipknot).round.toInt
    previous match {
      case GuessResult.guessGreater => end - randomizedMargin
      case GuessResult.guessLesser  => start + randomizedMargin
      case _                        => 50
    }
  }

  val strategies = Vector(basic, binary, feelingLucky, odds, slipknot, randomSlipknot)

  // maybe this could have been done better with macros. I'd prefer working reflection for this specific case.
  val deterministicStrategies = Vector(
    new NamedGuessStrategy(basic, "basic"),
    new NamedGuessStrategy(binary, "binary"),
    new NamedGuessStrategy(feelingLucky, "feelingLucky"),
    new NamedGuessStrategy(odds, "odds"),
    new NamedGuessStrategy(slipknot, "slipknot")
  )

  val randomizedStrategies = Vector(
    new NamedGuessStrategy(randomSlipknot, "randomSlipknot")
  )

  //I'll put this here...
  val strategyQualities = Vector((odds, 6), (binary, 11), (randomSlipknot, 12), (feelingLucky, 11), (slipknot, 11), (basic, 11))

  val weightedStrategies = strategyQualities.flatMap {
    case (s, q) => Iterator.continually { s }.take(q)
  }

  def getSome = myutils.random(weightedStrategies).getOrElse(randomSlipknot)

}

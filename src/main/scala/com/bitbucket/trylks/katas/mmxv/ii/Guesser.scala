package com.bitbucket.trylks.katas.mmxv.ii

import com.bitbucket.trylks.katas.mmxv.ii.GuessResult._
import scala.annotation.tailrec

class Guesser(val arbiter: Int => GuessResult, val strategy: GuessStrategies.guessStrategy) {

  private val totalAttempts = Configuration.getAttempts()
  private val defaultRange = Configuration.getRange()

  private def getPoints(attempt: Int, found: Boolean): Int = {
    val toconsider = attempt - (if (found) 1 else 0)
    100 - toconsider * 20
  }

  private def newRange(range: (Int, Int), candidate: Int, result: GuessResult): (Int, Int) = result match {
    case GuessResult.guessGreater => (candidate + 1, range._2)
    case GuessResult.guessLesser  => (range._1, candidate - 1)
    case GuessResult.guessEqual   => (candidate, candidate)
    case _                        => range
  }

  def guess(range: (Int, Int) = defaultRange) = guessInRange(defaultRange)

  @tailrec
  private def guessInRange(range: (Int, Int), attempt: Int = 1, previous: GuessResult = noGuess): Int = {
    val (start, end) = range
    val candidate = strategy(start, end, attempt, previous)
    val newGuess = arbiter(candidate)
    if (attempt == totalAttempts || newGuess == guessEqual)
      getPoints(attempt, newGuess == guessEqual)
    else
      guessInRange(newRange(range, candidate, newGuess), attempt + 1, newGuess)
  }
}

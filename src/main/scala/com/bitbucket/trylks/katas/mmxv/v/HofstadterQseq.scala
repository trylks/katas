package com.bitbucket.trylks.katas.mmxv.v

object HofstadterQseq extends App {
  val q: Stream[Int] = Stream(1, 1, 1) ++ (3 to Int.MaxValue).iterator.map { n =>
    q(n - q(n - 1)) + q(n - q(n - 2))
  }
  (1 to 10) foreach { i => println(s"Q($i) = ${q(i)}") }
  println(s"Q(1000) = ${q(1000)}")
}

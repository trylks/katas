package com.bitbucket.trylks.katas.mmxv.ii

import scala.compat.Platform
import scala.io.StdIn
import GuessResult._
import scala.annotation.tailrec
import scala.util.{ Try, Success }
import com.bitbucket.trylks.katas.myutils

object PickGuess {

  val (min, max) = Configuration.getRange()
  val totalAttempts = Configuration.getAttempts()

  def main(args: Array[String]): Unit = Try { args(0).toLowerCase() } match {
    case Success("pick")  => pick()
    case Success("guess") => guess()
    case _                => println("wat?") //TODO: do...
  }

  private def pick(): Unit = {
    val numberResults = Configuration.numberQualities.flatMap {
      case (n, q) => Iterator.continually { n }.take(q)
    }
    println(myutils.random(numberResults).getOrElse(90))
  }

  private def guess(): Unit = new Guesser(consoleArbiter, GuessStrategies.getSome).guess()

  private def consoleArbiter(candidate: Int): GuessResult = {
    println(candidate)
    StdIn.readLine() match {
      case "+" => guessGreater
      case "-" => guessLesser
      case "=" => guessEqual
      case _   => noGuess
    }
  }

}

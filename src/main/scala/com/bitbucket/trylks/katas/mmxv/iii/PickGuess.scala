package com.bitbucket.trylks.katas.mmxv.iii

import scala.compat.Platform
import scala.io.StdIn
import com.bitbucket.trylks.katas.mmxv.ii._
import com.bitbucket.trylks.katas.mmxv.ii.GuessResult._
import scala.annotation.tailrec
import scala.util.{ Try, Success }
import com.bitbucket.trylks.katas.myutils

object PickGuess {

  def main(args: Array[String]): Unit = Try { args(0).toLowerCase() } match {
    case Success("pick")  => Picker.pick()
    case Success("guess") => Guesser.guess()
    case _                => println("wat?") //TODO: do...
  }

}

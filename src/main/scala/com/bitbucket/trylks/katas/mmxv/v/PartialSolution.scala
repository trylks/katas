package com.bitbucket.trylks.katas.mmxv.v

trait PartialSolution {

  def isFinal: Boolean

  def children: Seq[PartialSolution]

  def backtracking(start: PartialSolution): Seq[PartialSolution] = backtracking(Seq(start))

  def backtracking(current: Seq[PartialSolution]): Seq[PartialSolution] =
    if (current.isEmpty)
      current
    else if (current.head.isFinal)
      current.head +: backtracking(current.tail)
    else
      backtracking(current.head.children ++ current.tail)

}


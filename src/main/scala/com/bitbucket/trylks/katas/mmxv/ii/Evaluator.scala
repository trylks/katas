package com.bitbucket.trylks.katas.mmxv.ii

import scala.collection.immutable.NumericRange.Inclusive
import com.bitbucket.trylks.katas.mmxv.ii.GuessResult.GuessResult
import GuessResult._
import scala.collection.GenTraversableOnce
import scala.collection.parallel.immutable.ParSeq

// dealines, priorities, opportunity cost, quality, sorry for this, don't look at it
// http://media.tumblr.com/tumblr_m66u1i1JIK1qlynf0.gif
// this is another kata on its own, one that I cannot really work on now
object Evaluator {

  val (min, max) = Configuration.getRange()
  val totalAttempts = Configuration.getAttempts()
  val deterministicStrategies = GuessStrategies.deterministicStrategies
  val randomizedStrategies = GuessStrategies.randomizedStrategies

  private def proavg[T](ts: Iterable[T])(implicit num: Numeric[T]) = (ts.sum, ts.size)

  def main(args: Array[String]): Unit = {

    val detEva: ParSeq[(Int, Vector[(GuessStrategies.NamedGuessStrategy, Int)])] = (min to max).par.map(
      n => (n, deterministicStrategies.map(
        s => (s, new Guesser(innerArbiter(n), s).guess()))))

    val ranEva = (min to max).par.map(
      n => (n, randomizedStrategies.map(
        s => (s, (0 to Configuration.getEvaluationRepetitions()).map(
          z => new Guesser(innerArbiter(n), s).guess())))))

    val numberQuality = (ranEva.map {
      case (n, vs) => (n, (vs.map {
        case (s, vsz) => vsz.max
      }.max))
    } ++ detEva.map {
      case (n, vs) => (n, vs.map(_._2).max)
    }).groupBy(_._1).map {
      case (n, rds) => (n, (100 - rds.map(_._2).max) / 10)
    }.toVector.sortBy(_._1)

    val proStrategyQuality: ParSeq[(GuessStrategies.NamedGuessStrategy, (Int, Int))] = ((ranEva.flatMap {
      case (n, vs) => vs.map {
        case (s, vsz) => (s, proavg(vsz))
      }
    }) ++ (detEva.flatMap {
      case (n, vs) => vs.map {
        case (s, v) => (s, (v, 1))
      }
    }))

    // I'm completely ignoring the overlap between strategies, but I really have no time for that.
    val strategyQuality = proStrategyQuality.groupBy(_._1).map {
      case (s, vs) => (s, vs.foldLeft((0, 0))({
        case ((presum, presize), (strat, (prosum, prosize))) => (presum + prosum, presize + prosize)
      }))
    }.map {
      case (s, (prosum, prosize)) => (s.name, (prosum.toDouble / prosize).round)
    }.toVector

    println(numberQuality)
    println(strategyQuality)
  }

  private def innerArbiter(goal: Int)(candidate: Int): GuessResult =
    goal.compare(candidate) match {
      case -1 => GuessResult.guessLesser
      case 0  => GuessResult.guessEqual
      case 1  => GuessResult.guessGreater
    }

}

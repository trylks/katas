package com.bitbucket.trylks.katas.mmxv.iii

import scala.io.StdIn
import com.bitbucket.trylks.katas.mmxv.ii.GuessResult._
import com.bitbucket.trylks.katas.mmxv.ii.Configuration
import com.bitbucket.trylks.katas.mmxv.ii.GuessResult
import scala.annotation.tailrec
import scala.util.{ Try, Success, Failure }
import scala.collection.TraversableLike
import com.bitbucket.trylks.katas.convenience._
import scala.language.implicitConversions

object Guesser {

  type Strategy = (Int, Seq[Candidate]) => Int

  case class Candidate(value: Int, guessed: Int, notGuessed: Int, failed: Int)

  type Weight = Candidate => Int

  val sniffer: Weight = _.notGuessed
  val piercer: Weight = _.guessed
  val perfectionistSniffer: Weight = x => x.notGuessed * 3 - x.failed
  val perfectionistPiercer: Weight = x => x.guessed * 5 - x.failed
  val wrath: Weight = x => x.notGuessed * 3 - x.guessed
  val ultimateEye: Weight = x => x.notGuessed * 5 - x.guessed - x.failed
  /*
  * implicit mind models:
  * sniffer: adversary learns from success
  * piercer: picker assumes that guesser corrects for picker learning (unlikely, I know)
  * perfectionistSniffer: adversary learns from success, corrected for less predictable exploration
  * perfectionistPiercer: adversary assumes guesser corrects for his learning, corrected for less predictable exploration
  * wrath: adversary learns from success and failures
  * ultimateEye: adversary learns from success and failures, corrected for less predictable exploration
  * */

  val basicValues = Seq(sniffer, piercer, perfectionistSniffer, perfectionistPiercer, wrath, ultimateEye)
  val allStrategies: Seq[Strategy] = basicValues.map(makeBasicStrat(_)) ++ basicValues.map(makeSlipknotStrat(_)_)

  private val attempts = Configuration.getAttempts
  private val (minv, maxv) = Configuration.getRange
  private val range = Configuration.getRange
  private val initialState = (minv to maxv).map(x => Candidate(x, 1, 1, 1))
  private val strategies: Selector[Strategy] = new Selector(allStrategies) // state, danger zone!

  def makeBasicStrat(weight: Weight): (Int, Seq[Candidate]) => Int =
    (a: Int, cs: Seq[Candidate]) => median(cs.map(x => (x.value, weight(x))))

  def makeSlipknotStrat(weight: Weight)(attempt: Int, candidates: Seq[Candidate]): Int = {
    val remaining = attempts - attempt
    val medianval = median(candidates.map(x => (x.value, weight(x))))
    val leftMargin = medianval - candidates.head.value
    val rightMargin = candidates.last.value - medianval
    val slipknotMargin = ((attempt * idealMargin(remaining) + remaining * Math.min(leftMargin, rightMargin)) / attempts).round.toInt
    if (leftMargin < rightMargin)
      candidates.head.value + slipknotMargin
    else if (rightMargin < leftMargin)
      candidates.last.value - slipknotMargin
    else
      medianval
  }

  def idealMargin(remaining: Int) = Math.pow(2, remaining) - 1

  private case class MyRichInt(i: Int) {
    def in(range: (Int, Int)) = range._1 <= i && i <= range._2
  }

  private implicit def intToRich(i: Int): MyRichInt = MyRichInt(i)

  @tailrec
  private def getResult(): GuessResult =
    StdIn.readLine match {
      case "+"        => guessGreater
      case "-"        => guessLesser
      case "="        => guessEqual
      case "<>" | ">" => noGuess
      case null       => throw new java.io.EOFException("Console has reached end of input")
      case _          => getResult()
    }

  @tailrec
  def guess(points: Int = 0, state: Seq[Candidate] = initialState): Int = {
    val (strategy, id) = strategies.choose()
    Try { guessIteration(state, strategy, range, 1) } match {
      case Success((n, s)) => {
        val update = if (n == 0) -1 else n / 20
        strategies.update(id, update * 5)
        strategies.update((id + basicValues.size) % allStrategies.size, update)
        guess(points + n, s)
      }
      case Failure(e) => {
        e.printStackTrace()
        points
      }
    }
  }

  def median[T](weightedElements: Seq[(T, Int)]): T = {
    val cumulative = weightedElements.tail.scanLeft(weightedElements.head._2)(_ + _._2)
    val ambiguous = cumulative.last % 2 == 0
    val medianWeight = (cumulative.last + 1) / 2
    val index = cumulative.takeWhile(_ < medianWeight).size
    val correctedIndex =
      if (ambiguous && medianWeight == cumulative(index) && weightedElements(index + 1)._2 > weightedElements(index)._2)
        index + 1
      else
        index
    weightedElements(correctedIndex)._1
  }

  @tailrec
  private def guessIteration(state: Seq[Candidate], strategy: Strategy, r: (Int, Int), attempt: Int): (Int, Seq[Candidate]) = {
    val candidate = strategy(attempt, state.filter(_.value in r)) // just because... Scala.
    println(candidate)
    val result = getResult()
    val newState = update(state, candidate, result, r, attempt)
    if (result == guessEqual)
      ((attempts - attempt + 1) * 20, newState)
    else if (attempt == attempts)
      (0, newState)
    else
      guessIteration(newState, strategy, rangeUpdate(result, r, candidate), attempt + 1)
  }

  def rangeUpdate(result: GuessResult, range: (Int, Int), candidate: Int): (Int, Int) = (result, range) match {
    case (GuessResult.guessGreater, (a, b)) => (candidate + 1, b)
    case (GuessResult.guessLesser, (a, b))  => (a, candidate - 1)
    case (GuessResult.guessEqual, (a, b))   => (candidate, candidate)
    case (_, e)                             => e
  }

  // the most macro-able function in the repo so far
  def update(state: Seq[Candidate], candidate: Int, result: GuessResult, range: (Int, Int), attempt: Int): Seq[Candidate] = {
    (if (result == guessEqual) {
      collens(state,
        (x: Candidate) => x.value == candidate,
        (y: Candidate) => y.copy(guessed = y.guessed + 1)
      )
    } else {
      val failed = collens(state,
        (x: Candidate) => x.value == candidate,
        (y: Candidate) => y.copy(failed = y.failed + 1)
      )
      if (attempt == attempts) {
        collens(failed,
          (x: Candidate) => x.value != candidate && (x.value in range),
          (y: Candidate) => y.copy(notGuessed = y.notGuessed + 1)
        )
      } else
        failed
    })
  }

  // This is not an implementation of anything resembling lenses, but it should be, maybe.
  def collens[T](col: TraversableLike[T, Seq[T]], select: T => Boolean, transform: T => T) =
    col.map(x => if (select(x)) transform(x) else x)

}

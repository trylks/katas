package com.bitbucket.trylks.katas.mmxv.iii

import com.bitbucket.trylks.katas.mmxv.ii.Configuration
import scala.io.StdIn
import scala.annotation.tailrec
import com.bitbucket.trylks.katas.myutils._
import scala.util.{ Try, Success, Failure }

object Picker {

  case class Candidate(value: Int, guessed: Int, notGuessed: Int)

  type Strategy = Seq[Candidate] => Seq[Candidate]

  val explore: Strategy = e => e.sortBy(x => (x.guessed + 1) * (x.guessed + x.notGuessed + 1)) // implicit mind model: adversary learns from not guessed values
  val exploit: Strategy = e => e.sortBy(x => (x.guessed + 1) / (x.guessed + x.notGuessed + 1.0)) // implicit mind model: adversary learns from guessed values, or does not learn

  val attempts = Configuration.getAttempts
  val range = Configuration.getRange
  val (minv, maxv) = range
  val strategies: Selector[Strategy] = new Selector(Seq(explore, exploit)) // state, danger zone!

  def pick(): Int = {
    val initial: Seq[Candidate] = (minv to maxv).map(Candidate(_, 0, 0)).toSeq
    pickLoop(bias(initial), 0)
  }

  @tailrec
  private def pickLoop(memory: Seq[Candidate], points: Int): Int = {
    val (next, strat) = truePick(memory)
    println(next.head.value)
    Try { guessedAt() } match {
      case Success(result) => {
        strategies.update(strat, result.fold(1)(_ - attempts - 1))
        val newpoints = points + 20 * result.fold(5)(_ - 1)
        pickLoop(next.tail :+ update(next.head, result), newpoints)
      }
      case Failure(e) => {
        e.printStackTrace()
        points
      }
    }
  }

  private def guessedAt(): Option[Int] = {
    val s = StdIn.readLine.toLowerCase
    if (s startsWith "not")
      None
    else
      (s split "at").lastOption.map(_.trim().toInt)
  }

  private def truePick(memory: Seq[Candidate]): (Seq[Candidate], Int) = {
    val scope = randomInt(range)
    val (inside, outside) = memory.partition(x => Math.abs(x.value - scope) <= 12)
    val (strategy, id) = strategies.choose()
    (strategy(inside) ++ outside, id)
  }

  private def update(picked: Candidate, result: Option[Int]): Candidate = result match {
    case None    => Candidate(picked.value, picked.guessed, picked.notGuessed + 1)
    case Some(n) => Candidate(picked.value, picked.guessed + attempts - n + 1, picked.notGuessed)
  }

  // the ugliness here, maybe some day I'll fix that...
  private def bias(initial: Seq[Candidate]): Seq[Candidate] = {

    def worse(c: Candidate, t: Int): Candidate = update(c, Some(attempts - t + 1))

    def negative(initial: Seq[Candidate], attempt: Int = attempts): Seq[Candidate] = {
      if (attempt == 0)
        return initial
      val (first, second) = initial.splitAt((initial.size - 1) / 2)
      val att = attempt - 1
      negative(first, att) ++ (worse(second.head, attempt) +: negative(second.tail, att))
    }

    def better(c: Candidate, t: Int) = (c /: (1 to t))((x, _) => update(x, None))

    def positive(initial: List[Candidate]): List[Candidate] = initial match {
      case one :: two :: list => (better(one, 2) :: better(two, 1) :: list)
      case whatever           => whatever
    }

    positive(positive(negative(initial).toList).reverse).toSeq
  }

}

package com.bitbucket.trylks.katas.iii

import org.scalatest.WordSpec
import com.bitbucket.trylks.katas.mmxv.iii.Guesser.median

class MedianTest extends WordSpec {
  "median" should {
    "be in the middle - left" in {
      assert("b" equals median(Seq(("a", 1), ("b", 2), ("c", 1), ("d", 1))))
    }
    "be in the middle - right" in {
      assert("c" equals median(Seq(("a", 1), ("b", 1), ("c", 2), ("d", 1))))
    }
    "be in the middle 2" in {
      assert("c" equals median(Seq(("a", 1), ("b", 2), ("c", 2), ("d", 2))))
    }
    "be the heaviest when ambiguous - left" in {
      assert("a" equals median(Seq(("a", 3), ("b", 1), ("c", 1), ("d", 1))))
    }
    "be the heaviest when ambiguous - left 2" in {
      assert("b" equals median(Seq(("a", 1), ("b", 2), ("c", 1), ("d", 2))))
    }
    "be the heaviest when ambiguous - right" in {
      assert("d" equals median(Seq(("a", 1), ("b", 1), ("c", 1), ("d", 3))))
    }
    "be the heaviest when ambiguous - right 2" in {
      assert("c" equals median(Seq(("a", 2), ("b", 1), ("c", 2), ("d", 1))))
    }
    "be sensible to heavy individuals" in {
      assert("d" equals median(Seq(("a", 1), ("b", 1), ("c", 1), ("d", 4))))
    }
    "not be too sensible to heavy individuals" in {
      assert("c" equals median(Seq(("a", 1), ("b", 1), ("c", 1), ("d", 2))))
    }

  }
}

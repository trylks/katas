# README #

This is an unconventional repository, as it may contain several small independent programs (programming katas). This bad practice may not last much.

## First kata - Number guessing and picking 

The program receives a single argument, "pick" or "guess" and then interacts with an arbiter, blah blah.

### How to run this? ###

Long story short, avoid `sbt run`, use `sbt assembly` at the root of the repo. This will generate a `jar` at `reporoot/target/scala-2.11/pickguess-assembly-1.0.jar`

Rut it with `java -jar thatthing.jar arg`. Where `arg` is either "pick" or "guess".

### Additional notes ###

Avoid the evaluation class. It's completely rushed.